﻿using System;
// *******************************
// ****   Welcome to my app   ****
// *******************************

// *******************************
// What is your name?

// Your name is:
namespace exercise16
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("*******************************");
            Console.WriteLine("****   Welcome to my app   ****");
            Console.WriteLine("*******************************");
            Console.WriteLine("");
            Console.WriteLine("*******************************");
            Console.WriteLine("What is your name?");
            var name = Console.ReadLine();
            Console.WriteLine($"your name is {name}");
        }
    }
}
